package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.service.VerificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/home")
public class QuestionsCheckController {

    private final VerificationService verificationService;

    @GetMapping(path = "/question")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity getQuestion(){
        Question questionDto = verificationService.getAllQuestion();
        return ResponseEntity.ok(questionDto.getQuestion());
    }

    @PostMapping(path = "/answer")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity getAnswer(@RequestBody Question question){
        boolean bool = verificationService.getQuestion(question);

        if(!bool) {
            RestTemplate restTemplate = new RestTemplate();

            String url = "http://localhost:8082/api/home/question";

            return ResponseEntity.ok(restTemplate.getForObject(url,String.class));
        }else {

        }

        return ResponseEntity.ok(bool);
    }

}
