package com.m46.codechecks.service;


import com.m46.codechecks.model.dto.Question;
import com.m46.codechecks.model.entity.QuestionEntity;
import com.m46.codechecks.repository.VerificationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;


import java.util.List;


@Service
@RequiredArgsConstructor
@Slf4j
public class VerificationService {

    private final VerificationRepository verificationRepository;

    private final ModelMapper modelMapper;

    public Boolean getQuestion(Question question) {
        QuestionEntity questionEntity = verificationRepository.findAllById(question.getId());

        if(!questionEntity.getAnswer().equals(question.getAnswer())) {
            return false;
        }
        return true;
    }

    public Question getAllQuestion() {
        List<QuestionEntity> list = verificationRepository.findAll();

        if(list.size() >= 0) {
            QuestionEntity question = list.get((int) (1+Math.random() * list.size()-1));
            Question questionDto = modelMapper.map(question,Question.class);
            return questionDto;
        }
        return null;
    }
}
