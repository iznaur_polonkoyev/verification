package com.m46.codechecks.controller;

import com.m46.codechecks.model.EmailVerification;
import com.m46.codechecks.model.EmailVerificationState;
import com.m46.codechecks.model.VerificationRequest;
import com.m46.codechecks.service.EmailVerificationService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/email/verify")
public class EmailVerificationController {
    private final EmailVerificationService verificationService;

    @GetMapping(path = "/{email}")
    @ResponseStatus(HttpStatus.OK)
    public EmailVerificationState getVerificationState(
            @PathVariable String email
    ){

        return verificationService.getVerificationState(email);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void requestEmailVerification(

            @RequestBody VerificationRequest verificationRequest
    ) {
        log.info("New verification for {} {}", verificationRequest.getFirstName(), verificationRequest.getEmail());
        verificationService.requestEmailVerification(
                verificationRequest.getEmail(),
                verificationRequest.getFirstName());
    }

    @PostMapping(path = "/check")
    @ResponseStatus(HttpStatus.OK)
    public void verify(

            @RequestBody EmailVerification verification
    ) {
        log.info("Verify {} with code '{}'", verification.getEmail(), verification.getVerificationCode());
        verificationService.verify(verification.getEmail(), verification.getVerificationCode());
//        if(bol){
//            RestTemplate restTemplate = new RestTemplate();
//
//            String url = "http://localhost:8082/api/home/question";
//           return ResponseEntity.ok(restTemplate.getForObject(url,String.class));
//        }
//         return ResponseEntity.ok("Error");
    }
}
