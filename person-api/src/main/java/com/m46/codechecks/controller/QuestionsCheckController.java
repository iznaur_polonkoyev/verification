package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.EmailVerifiCodeDto;
import com.m46.codechecks.model.dto.PersonDto;


import com.m46.codechecks.service.PersonService;
import com.m46.codechecks.service.ProductsService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/add")
public class QuestionsCheckController {

    private final PersonService personService;

    private final ProductsService productsService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity check(@RequestBody PersonDto personDto) {
        System.out.println("Зашел");
        String url ;
        EmailVerifiCodeDto question = new EmailVerifiCodeDto();
        question.setEmail(personDto.getEmail());
        question.setFirstName(personDto.getName());

        RestTemplate restTemplate = new RestTemplate();

        try {
             url = "http://localhost:8081/api/email/verify";

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<EmailVerifiCodeDto> entity = new HttpEntity<>(question, headers);
            restTemplate.postForObject(url, entity, String.class);

        }catch (RuntimeException e){
            throw new RuntimeException("ERROR");
        }


        PersonDto personDto1 = personService.addPerson(personDto);
        if(Objects.nonNull(personDto1)) {

            personDto.getProductsDto().setPerson_id(personDto1.getId());
            productsService.addProducts(personDto.getProductsDto());
            return ResponseEntity.ok(personDto1);
        }else {
            throw new RuntimeException("ERROR");
        }


    }
    @GetMapping(name = "/update")
    public void UpdateProduct(@RequestParam(value = "status",required = false) String status){

    }
}
