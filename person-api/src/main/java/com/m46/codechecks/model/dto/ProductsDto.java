package com.m46.codechecks.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductsDto {

    private int id;


    private String name;


    private String status;

    private int person_id;
}
