CREATE TABLE persons
(
    id         SERIAL PRIMARY KEY,
    email      CHARACTER VARYING(128) NOT NULL,
    name       CHARACTER VARYING(128) NOT NULL,
    surname    CHARACTER VARYING(128) NOT NULL,
    patronymic CHARACTER VARYING(128)
);

CREATE TABLE products
(
    id        SERIAL PRIMARY KEY,
    name      CHARACTER VARYING(128) NOT NULL,
    status    CHARACTER VARYING(128),
    person_id INTEGER references persons (id)
);

CREATE TABLE captcha
(
    id       SERIAL PRIMARY KEY,
    question CHARACTER VARYING(255) NOT NULL,
    answer   CHARACTER VARYING(255) NOT NULL
);

CREATE TABLE email_verifications
(
    verification_id     SERIAL PRIMARY KEY,
    email               CHARACTER VARYING(128)                    NOT NULL,
    created_at          TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    verification_code   CHARACTER VARYING(16)                     NOT NULL,
    verification_status CHARACTER VARYING(16),
    message             CHARACTER VARYING(256)
);